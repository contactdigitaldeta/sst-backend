package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Application.sst_crm.restapi.entity.CourtExpences;


public interface ICourtExpences extends JpaRepository<CourtExpences, Long> {

}
