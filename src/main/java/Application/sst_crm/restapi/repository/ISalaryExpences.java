package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import Application.sst_crm.restapi.entity.SalaryExpences;

public interface ISalaryExpences extends JpaRepository<SalaryExpences, Long> {

}
