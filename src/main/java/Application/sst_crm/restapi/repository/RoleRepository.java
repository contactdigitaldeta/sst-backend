package Application.sst_crm.restapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Application.sst_crm.restapi.entity.ERole;
import Application.sst_crm.restapi.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Optional<Role> findByRoleName(ERole roleName);
	
}
