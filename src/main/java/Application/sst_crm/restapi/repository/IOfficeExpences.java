package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Application.sst_crm.restapi.entity.OfficeExpences;


public interface IOfficeExpences extends JpaRepository<OfficeExpences, Long>{

}
