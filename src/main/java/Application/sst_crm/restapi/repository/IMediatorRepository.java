package Application.sst_crm.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import Application.sst_crm.restapi.entity.Mediator;

public interface IMediatorRepository extends JpaRepository<Mediator, Long> {
	
static String allmediators ="SELECT name FROM sst_crm.mediator";
	
	@Query(value=allmediators, nativeQuery=true)
	List<String> findAllName();
	

}
