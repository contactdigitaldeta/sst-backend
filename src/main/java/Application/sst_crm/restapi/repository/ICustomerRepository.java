package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Application.sst_crm.restapi.entity.Customer;


public interface ICustomerRepository extends JpaRepository<Customer, Long>{

}
