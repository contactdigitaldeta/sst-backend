package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Application.sst_crm.restapi.entity.Lead;

public interface ILeadRepository extends JpaRepository<Lead, Long> {

}



