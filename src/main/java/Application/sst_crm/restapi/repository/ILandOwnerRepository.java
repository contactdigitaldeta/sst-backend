package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Application.sst_crm.restapi.entity.LandOwner;

public interface ILandOwnerRepository extends JpaRepository<LandOwner, Long> {

}
