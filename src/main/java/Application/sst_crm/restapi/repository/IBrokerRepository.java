package Application.sst_crm.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import Application.sst_crm.restapi.entity.Broker;

public interface IBrokerRepository extends JpaRepository<Broker, Long>{
	
	static String allbrokers ="SELECT name FROM sst_crm.broker";
	
	@Query(value=allbrokers, nativeQuery=true)
	List<String> findAllName();
	
	

}
