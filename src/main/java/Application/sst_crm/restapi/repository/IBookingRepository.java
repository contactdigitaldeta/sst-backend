package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import Application.sst_crm.restapi.entity.Booking;

public interface IBookingRepository extends JpaRepository<Booking, Long>{

}
