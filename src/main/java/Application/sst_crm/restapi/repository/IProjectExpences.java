package Application.sst_crm.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import Application.sst_crm.restapi.entity.ProjectExpences;

public interface IProjectExpences extends JpaRepository<ProjectExpences, Long>{

}
