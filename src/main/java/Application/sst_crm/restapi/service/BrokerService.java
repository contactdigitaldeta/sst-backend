package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import Application.sst_crm.restapi.dto.BrokerDTO;
import Application.sst_crm.restapi.entity.Broker;
import Application.sst_crm.restapi.repository.IBrokerRepository;


@Service
public class BrokerService {
	
	@Autowired
	private IBrokerRepository brokerRepository;
	
	public List<Broker>getBrokers(){
		return brokerRepository.findAll();
	}
	
	public String insertBroker(BrokerDTO broker) {
		brokerRepository.saveAndFlush(BrokerDTO.prepareBrokerEntity(broker));
		return "Broker Added Successfully";	
	}
	
	public Optional<Broker>getBrokerById(Long id){
		return brokerRepository.findById(id);
	}
	
	public String updateBroker(BrokerDTO broker, Long id ) {
		Optional<Broker> optionalBroker = brokerRepository.findById(id);
		
		Broker brokerEntity = optionalBroker.get();
		
		brokerEntity.setName(broker.getName());
		brokerEntity.setFathersName(broker.getFathersName());
		brokerEntity.setPermanentAddress(broker.getPermanentAddress());
		brokerEntity.setCity(broker.getCity());
		brokerEntity.setState(broker.getState());
		brokerEntity.setPostalCode(broker.getPostalCode());
		brokerEntity.setEmailId(broker.getEmailId());
		brokerEntity.setMobileNumber(broker.getMobileNumber());
		brokerEntity.setAlternateMobileNumber(broker.getAlternateMobileNumber());
		
		brokerRepository.saveAndFlush(brokerEntity);
		
		return "Broker updated sucessfully.";
		
		}
		
		public String deleteBroker(Long id) {
		 Optional <Broker> optionalBroker = brokerRepository.findById(id);
		 if(optionalBroker != null) {
			 brokerRepository.deleteById(id);
		 }
		 
		return "Broker Deleted SucessFully"+ optionalBroker.get().getName()+", "+ optionalBroker.get().getId();
		}
		 
		 public Long getBrokerCount() {
			 return  brokerRepository.count();
		 }
		 
		 public List<String> getAllbroker() {
				return brokerRepository.findAllName();
			}
		 

}
