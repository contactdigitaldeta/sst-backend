package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import Application.sst_crm.restapi.dto.BookingDTO;
import Application.sst_crm.restapi.entity.Booking;
import Application.sst_crm.restapi.entity.Installments;
import Application.sst_crm.restapi.repository.IBookingRepository;

@Service
public class BookingService {
	
	@Autowired
	private IBookingRepository bookingRepository;
	
	public List<Booking>getBookings(){
		return bookingRepository.findAll();
	}
	
	public String insertBooking(BookingDTO booking){
		Booking prepareBookingEntity = BookingDTO.prepareBookingEntity(booking);
		List<Installments> installmentsList = prepareBookingEntity.getInstallments();
		installmentsList.forEach(s->s.setBookingEntity(prepareBookingEntity));
		bookingRepository.saveAndFlush(prepareBookingEntity);
		return "Booking Added Successfully";
	}
	
	public Optional<Booking>getBookingById(Long id){
		return bookingRepository.findById(id);
	}
	
	public String updateBooking(BookingDTO booking, Long id ) {
		Optional<Booking> optionalBooking = bookingRepository.findById(id);
		
		Booking bookingEntity = optionalBooking.get();
		bookingEntity.setFirstName(booking.getFirstName());
		bookingEntity.setMiddleName(booking.getMiddleName());
		bookingEntity.setLastName(booking.getLastName());
		bookingEntity.setFathersName(booking.getFathersName());
		bookingEntity.setPermanentAddress(booking.getPermanentAddress());
		bookingEntity.setCorrespondenceAddress(booking.getCorrespondenceAddress());
		bookingEntity.setCity(booking.getCity());
		bookingEntity.setState(booking.getState());
		bookingEntity.setPostalCode(booking.getPostalCode());
		bookingEntity.setEmailId(booking.getEmailId());
		bookingEntity.setMobileNumber(booking.getMobileNumber());
		bookingEntity.setAlternateMobileNumber(booking.getAlternateMobileNumber());
		bookingEntity.setDateOfBirth(booking.getDateOfBirth());
		bookingEntity.setOccupation(booking.getOccupation());
		bookingEntity.setMediatorName(booking.getMediatorName());
		bookingEntity.setCommissionAmount(booking.getCommissionAmount());
		bookingEntity.setReference(booking.getReference());
		bookingEntity.setSchemeName(booking.getSchemeName());
		bookingEntity.setOptionForMoodOfPayment(booking.getOptionForMoodOfPayment());
		bookingEntity.setCornerPlot(booking.getCornerPlot());
		bookingEntity.setTotalCostRS(booking.getTotalCostRS());
		bookingEntity.setAdvanceRS(booking.getAdvanceRS());
		bookingEntity.setMonthlyScheme(booking.getMonthlyScheme());
		bookingEntity.setMoneyReceiptNumber(booking.getMoneyReceiptNumber());
		bookingEntity.setPlotSize(booking.getPlotSize());
		bookingEntity.setBookingDate(booking.getBookingDate());
		bookingEntity.setAgreementDate(booking.getAgreementDate());
		bookingEntity.setIntroduced(booking.getIntroduced());
		bookingEntity.setVerifiedBy(booking.getVerifiedBy());
		
//		List<Installments> installmentsListEntity = bookingEntity.getInstallments();
//		
		List<Installments> installmentsList = booking.getInstallmentsList();
		installmentsList.forEach(s->s.setBookingEntity(bookingEntity));
//		
//		for (Installments installment : installmentsListEntity) {
//			
//			installment.setDate(installmentsList);
//			installment.setChequeNo(id);
//			installment.setBookingEntity(bookingEntity);
//			installment.setAmount(id);
//			installment.setModeOfPayment(null);
//				
//		}
		bookingEntity.setInstallments(booking.getInstallmentsList());
		bookingRepository.saveAndFlush(bookingEntity);
		
		return "Booking updated sucessfully.";
		
		}
		
		public String deleteBooking(Long id) {
		 Optional <Booking> optionalBooking = bookingRepository.findById(id);
		 if(optionalBooking != null) {
			 bookingRepository.deleteById(id);
		 }
		 
		return "Booking Deleted SucessFully"+ optionalBooking.get().getFirstName()+", "+ optionalBooking.get().getId();
		}
		 
		 public Long getBookingsCount() {
			 return  bookingRepository.count();
			 
		 }
		 
}

	

	


