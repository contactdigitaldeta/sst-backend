package Application.sst_crm.restapi.service;

import java.util.List;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import Application.sst_crm.restapi.dto.LandOwnerDTO;
import Application.sst_crm.restapi.entity.Brokerinstallments;
import Application.sst_crm.restapi.entity.LandInstallments;
import Application.sst_crm.restapi.entity.LandOwner;
import Application.sst_crm.restapi.repository.ILandOwnerRepository;


@Service

public class LandOwnerService {

	@Autowired
	private ILandOwnerRepository landownerRepository;
	
	public List<LandOwner>getLandOwners(){
		return landownerRepository.findAll();
	}
	
	public String insertLandOwner(LandOwnerDTO landowner) {
		LandOwner  prepareLandOwnerEntity = LandOwnerDTO.prepareLandOwnerEntity(landowner);
		List<Brokerinstallments> brokerinstallmentslist=prepareLandOwnerEntity.getBrokerinstallments();
		brokerinstallmentslist.forEach(s->s.setLandOwnerEntity(prepareLandOwnerEntity));
		List<LandInstallments> landinstallmentsList= prepareLandOwnerEntity.getLandinstallments();
		landinstallmentsList.forEach(s->s.setLandOwnerEntity(prepareLandOwnerEntity));
		landownerRepository.saveAndFlush(prepareLandOwnerEntity);
		return "LandOwner Added Successfully";	
	}
	
	public Optional<LandOwner>getLandOwnerById(Long id){
		return landownerRepository.findById(id);
	}
	
	public String updateLandOwner(LandOwnerDTO landowner, Long id ) {
		Optional<LandOwner> optionalLandOwner = landownerRepository.findById(id);
		
		LandOwner landownerEntity = optionalLandOwner.get();
		
		landownerEntity.setName(landowner.getName());
		landownerEntity.setFathersName(landowner.getFathersName());
		landownerEntity.setPermanentAddress(landowner.getPermanentAddress());
		landownerEntity.setPostalCode(landowner.getPostalCode());
		landownerEntity.setCorrespondenceAddress(landowner.getCorrespondenceAddress());
		landownerEntity.setEmailId(landowner.getEmailId());
		landownerEntity.setMobileNumber(landowner.getMobileNumber());
		landownerEntity.setAlternateMobileNumber(landowner.getAlternateMobileNumber());
		landownerEntity. setDateOfBirth(landowner.getDateOfBirth());
		landownerEntity. setArea(landowner.getArea());
		landownerEntity .setLandPlotNumber(landowner.getLandPlotNumber());
		landownerEntity .setKhataNumber(landowner.getKhataNumber());
		landownerEntity.setLandTotalCost(landowner.getLandTotalCost());
		landownerEntity.setMonthlyScheme(landowner.getMonthlyScheme());
		landownerEntity.setAdvanceRupees(landowner.getAdvanceRupees());
		landownerEntity.setBookingDate(landowner.getBookingDate());
		landownerEntity.setAgreementDate(landowner.getAgreementDate());
		landownerEntity.setIntroduced(landowner.getIntroduced());
		landownerEntity.setVerify(landowner.getVerify());
		landownerEntity.setSelectDistricts(landowner.getSelectDistricts());
		landownerEntity.setSelectTahasil(landowner.getSelectTahasil());
		landownerEntity.setSelectVillage(landowner.getSelectVillage());
		landownerEntity.setRiCircle(landowner.getRiCircle());
		landownerEntity.setKhatiyanNumber(landowner.getKhatiyanNumber());
		landownerEntity.setBrokerName(landowner.getBrokerName());;
		landownerEntity.setTotalBrokerCommission(landowner.getTotalBrokerCommission());
		landownerEntity.setAdvancePayment(landowner.getAdvancePayment());
		landownerEntity.setDueAmount(landowner.getDueAmount());
		landownerEntity.setDueDate(landowner.getDueDate());
		landownerEntity.setOccupation(landowner.getOccupation());
		
		List<LandInstallments> installmentsList= landowner.getLandinstallmentslist();
		installmentsList.forEach(s->s.setLandOwnerEntity(landownerEntity));
		
		List<Brokerinstallments> brokerinstallmentslist=landowner.getBrokerinstallmentslist();
		brokerinstallmentslist.forEach(s->s.setLandOwnerEntity(landownerEntity));
		
		landownerEntity.setBrokerinstallments(landowner.getBrokerinstallmentslist());
		landownerEntity.setLandinstallments(landowner.getLandinstallmentslist());
		landownerRepository.saveAndFlush(landownerEntity);
		
		
		return "LandOwner updated sucessfully.";
		
	}
	
	public String deleteLandOwner(Long id) {
	 Optional <LandOwner> optionalLandOwner = landownerRepository.findById(id);
	 if(optionalLandOwner != null) {
		 landownerRepository.deleteById(id);
	 }
	 
	return "LandOwner Deleted SucessFully"+ optionalLandOwner.get().getName()+", "+ optionalLandOwner.get().getId();
	}
	 
	 public Long getLandOwnerCount() {
		 return  landownerRepository.count();
	 }
}
