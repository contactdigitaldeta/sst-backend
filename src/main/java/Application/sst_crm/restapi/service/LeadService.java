package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.LeadDTO;
import Application.sst_crm.restapi.entity.Lead;
import Application.sst_crm.restapi.repository.ILeadRepository;

@Service
public class LeadService {
	
	@Autowired
	private ILeadRepository leadRepository;
	
	public List<Lead>getLeads(){
		return leadRepository.findAll();
	}
	
	public String insertLead(LeadDTO lead) {
		leadRepository.saveAndFlush(LeadDTO.prepareLeadEntity(lead));
		return "Lead Added Sucessfully";
	}
	
	public Optional<Lead>getLeadById(Long id){
		return leadRepository.findById(id);
	}
	
	public String updateLead(LeadDTO lead, Long id) {
		Optional<Lead> optionalLead =leadRepository.findById(id);
		
		Lead leadEntity = optionalLead.get();
		
		leadEntity.setLeadType(lead.getLeadType());
		leadEntity.setFirstName(lead.getFirstName());
		leadEntity.setMiddleName(lead.getMiddleName());
		leadEntity.setLastName(lead.getLastName());
		leadEntity.setPhoneNumber(lead.getPhoneNumber());
		leadEntity.setAddress(lead.getAddress());
		leadEntity.setCity(lead.getCity());
		leadEntity.setState(lead.getState());
		leadEntity.setPinCode(lead.getPinCode());
		leadEntity.setCompanyName(lead.getCompanyName());
		leadEntity.setDesignation(lead.getDesignation());
		leadEntity.setSource(lead.getSource());
		leadEntity.setAssignTo(lead.getAssignTo());
		leadEntity.setStatus(lead.getStatus());
		leadRepository.saveAndFlush(leadEntity);
		
		return "Lead updated sucessfully";
		
		
	}
	
	public String deleteLead(Long id) {
		 Optional <Lead> optionalLead = leadRepository.findById(id);
		 if(optionalLead != null) {
			 leadRepository.deleteById(id);
		 }
		 
		return "Lead Deleted SucessFully "+ " "+ optionalLead.get().getFirstName()+", "+ optionalLead.get().getId();
		}
		 
		 public Long getLeadsCount() {
			 return  leadRepository.count();
	

}

}