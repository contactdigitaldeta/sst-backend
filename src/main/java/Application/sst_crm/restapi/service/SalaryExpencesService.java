package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import Application.sst_crm.restapi.dto.SalaryExpencesDTO;
import Application.sst_crm.restapi.entity.SalaryExpences;
import Application.sst_crm.restapi.repository.ISalaryExpences;

@Service
public class SalaryExpencesService {
	@Autowired
	private ISalaryExpences salaryExpencesRepository;
	
	public List<SalaryExpences>getSalaryExpences() {
		return salaryExpencesRepository.findAll();
	}
	public String insertSalaryExpences(SalaryExpencesDTO salaryExpences) {
		salaryExpencesRepository.saveAndFlush(SalaryExpencesDTO.prepareSalaryExpencesEntity(salaryExpences));
	return "Salaryexpenses Added Successfully";
	}
	
	public Optional<SalaryExpences> getSalaryExpencesById(Long id){
		return salaryExpencesRepository.findById(id);
	}
	
	public String updateSalaryExpences(SalaryExpencesDTO salaryExpences, Long id ) {
		Optional<SalaryExpences> optionalSalaryExpences =salaryExpencesRepository.findById(id);
		

		SalaryExpences salaryExpencesEntity = optionalSalaryExpences.get();
		salaryExpencesEntity.setPersonName(salaryExpences.getPersonName());
		salaryExpencesEntity.setDateOfPayment(salaryExpences.getDateOfPayment());
		salaryExpencesEntity.setAmount(salaryExpences.getAmount());
		salaryExpencesEntity.setMoodOfPayment(salaryExpences.getMoodOfPayment());
		salaryExpencesEntity.setPurposeOfPayment(salaryExpences.getPersonName());
		salaryExpencesEntity.setReferenceNo(salaryExpences.getReferenceNo());
		
		salaryExpencesRepository.saveAndFlush(salaryExpencesEntity);
		return "SalaryExpenses updated sucessfully";
	}
	
	public String deleteSalaryExpences(Long id) {
		Optional<SalaryExpences> optionalSalaryExpences = salaryExpencesRepository.findById(id);
		if(optionalSalaryExpences != null) {
			salaryExpencesRepository.deleteById(id);
		}
		
		return " salaryExpenses deleted sucessfully" + optionalSalaryExpences.get().getPersonName() + ","+ optionalSalaryExpences.get().getId();
	}
}

