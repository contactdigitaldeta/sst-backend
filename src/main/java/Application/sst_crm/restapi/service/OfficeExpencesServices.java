package Application.sst_crm.restapi.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.OfficeExpencesDTO;
import Application.sst_crm.restapi.entity.OfficeExpences;
import Application.sst_crm.restapi.repository.IOfficeExpences;
@Service
public class OfficeExpencesServices {
	@Autowired
	private IOfficeExpences officeExpencesRepository;
	
	public List<OfficeExpences>getOfficeExpences() {
		return officeExpencesRepository.findAll();
	}
	public String insertOfficeExpences(OfficeExpencesDTO officeExpences) {
		officeExpencesRepository.saveAndFlush(OfficeExpencesDTO.prepareOfficeExpencesEntity(officeExpences));
	return "OfficeExpences Added Successfully";
	}
	
	public Optional<OfficeExpences> getOfficeExpencesById(Long id){
		return officeExpencesRepository.findById(id);
	}
	
	public String updateOfficeExpences(OfficeExpencesDTO officeExpences, Long id ) {
		Optional<OfficeExpences> optionalOfficeExpences = officeExpencesRepository.findById(id);
		

		OfficeExpences officeExpencesEntity = optionalOfficeExpences.get();
		officeExpencesEntity.setPersonName(officeExpences.getPersonName());
		officeExpencesEntity.setDateOfPayment(officeExpences.getDateOfPayment());
		officeExpencesEntity.setAmount(officeExpences.getAmount());
		officeExpencesEntity.setMoodOfPayment(officeExpences.getMoodOfPayment());
		officeExpencesEntity.setPurposeOfPayment(officeExpences.getPersonName());
		officeExpencesEntity.setReferenceNo(officeExpences.getReferenceNo());
		
		officeExpencesRepository.saveAndFlush(officeExpencesEntity);
		return "OfficeExpences updated sucessfully";
	}
	
	public String deleteOfficeExpences(Long id) {
		Optional<OfficeExpences> optionalOfficeExpences = officeExpencesRepository.findById(id);
		if(optionalOfficeExpences != null) {
			officeExpencesRepository.deleteById(id);
		}
		
		return " officeExpences deleted sucessfully" + optionalOfficeExpences.get().getPersonName() + ","+ optionalOfficeExpences.get().getId();
	}
}

