package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.CustomerDTO;
import Application.sst_crm.restapi.entity.Customer;
import Application.sst_crm.restapi.repository.ICustomerRepository;


@Service
public class CustomerService {

	@Autowired
	private ICustomerRepository customerRepository;
	
	public List<Customer>getCustomers(){
		return customerRepository.findAll();
	}
	
	public String insertCustomer(CustomerDTO customer) {
		customerRepository.saveAndFlush(CustomerDTO.prepareCustomerEntity(customer));
		return "Customer Added Successfully";	
	}
	
	public Optional<Customer>getCustomerById(Long id){
		return customerRepository.findById(id);
	}
	
	public String updateCustomer(CustomerDTO customer, Long id ) {
		Optional<Customer> optionalCustomer = customerRepository.findById(id);
		
		Customer customerEntity = optionalCustomer.get();
		
		
		customerEntity.setFirstName(customer.getFirstName());
		customerEntity.setMiddleName(customer.getMiddleName());
		customerEntity.setLastName(customer.getLastName());
		customerEntity.setFathersName(customer.getFathersName());
		customerEntity.setPermanentAddress(customer.getPermanentAddress());
		customerEntity.setCorrespondenceAddress(customer.getCorrespondenceAddress());
		customerEntity.setCity(customer.getCity());
		customerEntity.setState(customer.getState());
		customerEntity.setPostalCode(customer.getPostalCode());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setMobileNumber(customer.getMobileNumber());
		customerEntity.setAlternateMobileNumber(customer.getAlternateMobileNumber());
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		customerEntity.setOccupation(customer.getOccupation());
		customerEntity.setDateOfBooking(customer.getDateOfBooking());
		customerRepository.saveAndFlush(customerEntity);
		return "Customer updated sucessfully.";
	}
	public String deleteCustomer(Long id) {
		 Optional <Customer> optionalCustomer = customerRepository.findById(id);
		 if(optionalCustomer != null) {
			 customerRepository.deleteById(id);
		 }
		 
		return "Customer Deleted SucessFully"+ optionalCustomer.get().getFirstName()+", "+ optionalCustomer.get().getId();
		}
		 
		 public Long getCustomerCount() {
			 return  customerRepository.count();
		 }
		 
}
