package Application.sst_crm.restapi.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.ProjectExpencesDTO;
import Application.sst_crm.restapi.entity.PexpenseSlot;
import Application.sst_crm.restapi.entity.ProjectExpences;
import Application.sst_crm.restapi.repository.IProjectExpences;

@Service
public class ProjectExpencesService {

	@Autowired
	private IProjectExpences projectExpencesRepository;
	
	
	
	public List<ProjectExpences>getProjectExpences() {
		return projectExpencesRepository.findAll();
	}
	
	public String insertProjectExpences(ProjectExpencesDTO projectExpences) {
		
		ProjectExpences prepareProjectExpencesEntity=ProjectExpencesDTO.prepareProjectExpencesEntity(projectExpences);
		
		List<PexpenseSlot> pexpenseslotslist = prepareProjectExpencesEntity.getPexpenseslots();
		
		pexpenseslotslist.forEach(s->s.setProjectExpenseEntity(prepareProjectExpencesEntity));
		
	    projectExpencesRepository.saveAndFlush(prepareProjectExpencesEntity);
	    
	return "ProjectExpences Added Successfully";
	}
	
	public Optional<ProjectExpences> getProjectExpencesById(Long id){
		return projectExpencesRepository.findById(id);
	}
	
	public String updateProjectExpences(ProjectExpencesDTO projectExpences, Long id ) {
		Optional<ProjectExpences> optionalProjectExpences = projectExpencesRepository.findById(id);
		
		ProjectExpences projectExpencesEntity = optionalProjectExpences.get();
		projectExpencesEntity.setProjectName(projectExpences.getProjectName());
		projectExpencesEntity.setDateOfStart(projectExpences.getDateOfStart());
		projectExpencesEntity.setLocation(projectExpences.getLocation());
		
		List<PexpenseSlot>pexpenseslotslist=projectExpences.getPexpenseslotslist();
		pexpenseslotslist.forEach(s->s.setProjectExpenseEntity(projectExpencesEntity));
		projectExpencesEntity.setPexpenseslots(projectExpences.getPexpenseslotslist());
		
		projectExpencesRepository.saveAndFlush(projectExpencesEntity);
		return "ProjectExpences updated sucessfully";
	}
	
	public String deleteProjectExpences(Long id) {
		Optional<ProjectExpences> optionalProjectExpences = projectExpencesRepository.findById(id);
		if(optionalProjectExpences != null) {
			projectExpencesRepository.deleteById(id);
		}
		
		return " projectExpences deleted sucessfully" + optionalProjectExpences.get().getProjectName() + ","+ optionalProjectExpences.get().getId();
	}
	
}
