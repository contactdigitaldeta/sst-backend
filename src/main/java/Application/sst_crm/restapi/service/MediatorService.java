package Application.sst_crm.restapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.MediatorDTO;
import Application.sst_crm.restapi.entity.Mediator;
import Application.sst_crm.restapi.repository.IMediatorRepository;

@Service
public class MediatorService {
	
	@Autowired
	private IMediatorRepository mediatorRepository;
	
	public List<Mediator>getMediators(){
		return mediatorRepository.findAll();
	}
	
	public String insertMediator(MediatorDTO mediator) {
		mediatorRepository.saveAndFlush(MediatorDTO.prepareMediatorEntity(mediator));
		return "Mediator Added Successfully";	
	}
	
	public Optional<Mediator>getMediatorById(Long id){
		return mediatorRepository.findById(id);
	}
	
	public String updateMediator(MediatorDTO mediator, Long id ) {
		Optional<Mediator> optionalMediator = mediatorRepository.findById(id);
		
		Mediator mediatorEntity = optionalMediator.get();
		
		mediatorEntity.setName(mediator.getName());
		mediatorEntity.setFathersName(mediator.getFathersName());
		mediatorEntity.setPermanentAddress(mediator.getPermanentAddress());
		mediatorEntity.setCity(mediator.getCity());
		mediatorEntity.setState(mediator.getState());
		mediatorEntity.setPostalCode(mediator.getPostalCode());
		mediatorEntity.setEmailId(mediator.getEmailId());
		mediatorEntity.setMobileNumber(mediator.getMobileNumber());
		mediatorEntity.setAlternateMobileNumber(mediator.getAlternateMobileNumber());
		
		mediatorRepository.saveAndFlush(mediatorEntity);
		
		return "Mediator updated sucessfully.";
		
		}
		
		public String deleteMediator(Long id) {
		 Optional <Mediator> optionalMediator = mediatorRepository.findById(id);
		 if(optionalMediator != null) {
			 mediatorRepository.deleteById(id);
		 }
		 
		return "Mediator Deleted SucessFully"+ optionalMediator.get().getName()+", "+ optionalMediator.get().getId();
		}
		 
		 public Long getMediatorCount() {
			 return  mediatorRepository.count();
		 }
		 
		 public List<String> getAllmediator() {
				return mediatorRepository.findAllName();
			}
		 

}
