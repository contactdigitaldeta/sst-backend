package Application.sst_crm.restapi.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Application.sst_crm.restapi.dto.CourtExpencesDTO;
import Application.sst_crm.restapi.entity.CourtExpences;
import Application.sst_crm.restapi.repository.ICourtExpences;

@Service
public class CourtExpencesService {
	
	@Autowired
	private ICourtExpences courtExpencesRepository;
	
	public List<CourtExpences>getCourtExpences() {
		return courtExpencesRepository.findAll();
	}
	public String insertCourtExpences(CourtExpencesDTO courtExpences) {
		courtExpencesRepository.saveAndFlush(CourtExpencesDTO.prepareCourtExpencesEntity(courtExpences));
	return "CourtExpences Added Successfully";
	}
	
	public Optional<CourtExpences> getCourtExpencesById(Long id){
		return courtExpencesRepository.findById(id);
	}
	
	public String updateCourtExpences(CourtExpencesDTO courtExpences, Long id ) {
		Optional<CourtExpences> optionalCourtExpences = courtExpencesRepository.findById(id);
		
		CourtExpences courtExpencesEntity = optionalCourtExpences.get();
		courtExpencesEntity.setPersonName(courtExpences.getPersonName());
		courtExpencesEntity.setDateOfPayment(courtExpences.getDateOfPayment());
		courtExpencesEntity.setAmount(courtExpences.getAmount());
		courtExpencesEntity.setMoodOfPayment(courtExpences.getMoodOfPayment());
		courtExpencesEntity.setPurposeOfPayment(courtExpences.getPersonName());
		courtExpencesEntity.setReferenceNo(courtExpences.getReferenceNo());
		
		courtExpencesRepository.saveAndFlush(courtExpencesEntity);
		return "CourtExpences updated sucessfully";
	}
	
	public String deleteCourtExpences(Long id) {
		Optional<CourtExpences> optionalCourtExpences = courtExpencesRepository.findById(id);
		if(optionalCourtExpences != null) {
			courtExpencesRepository.deleteById(id);
		}
		
		return " courtExpences deleted sucessfully" + optionalCourtExpences.get().getPersonName() + ","+ optionalCourtExpences.get().getId();
	}
}
