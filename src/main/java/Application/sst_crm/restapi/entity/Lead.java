package Application.sst_crm.restapi.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.LeadDTO;
import lombok.ToString;


@Data
@Entity
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Lead {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "leadId")
	Long Id;
	Integer leadType;
	String firstName;
	String middleName;
	String lastName;
	Long phoneNumber;
	String address;
	String city;
	String state;
	Integer pinCode;
	String companyName;
	String designation;
	String source;
	String assignTo;
	String status;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	
	public static LeadDTO prepareLeadDTO(Lead lead) {
		   return LeadDTO.builder()
				   .leadType(lead.getLeadType())
				   .firstName(lead.getFirstName())
				   .middleName(lead.getMiddleName())
				   .lastName(lead.getLastName())
				   .phoneNumber(lead.getPhoneNumber())
				   .address(lead.getAddress())
				   .city(lead.getCity())
				   .state(lead.getState())
				   .pinCode(lead.getPinCode())
				   .companyName(lead.getCompanyName())
				   .designation(lead.getDesignation())
				   .source(lead.getSource())
				   .assignTo(lead.getAssignTo())
				   .status(lead.getStatus())
				   .creationTt(lead.getCreationTt())
				   .build();

}
}