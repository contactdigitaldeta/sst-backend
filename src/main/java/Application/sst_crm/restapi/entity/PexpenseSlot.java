package Application.sst_crm.restapi.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import Application.sst_crm.restapi.dto.PexpenseSlotDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PexpenseSlot {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PExpenceSlotId")
	Long Id;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String modeOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	@JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="projectExpenceId")
	ProjectExpences ProjectExpenseEntity;
	
	public static PexpenseSlotDTO preparePexpenseSlotDTO(PexpenseSlot pexpenseslot) {
		
		return  PexpenseSlotDTO.builder()
				.dateOfPayment(pexpenseslot.getDateOfPayment())
				.amount(pexpenseslot.getAmount())
				.modeOfPayment(pexpenseslot.getModeOfPayment())
    			.purposeOfPayment(pexpenseslot.getPurposeOfPayment())
    			.referenceNo(pexpenseslot.getReferenceNo())
    			.build();
		
	}
}
