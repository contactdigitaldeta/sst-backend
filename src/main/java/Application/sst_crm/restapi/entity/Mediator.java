package Application.sst_crm.restapi.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.MediatorDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mediator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mediatorId")
	Long Id;
	String name;
	String fathersName;
	String permanentAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	
	public static MediatorDTO prepareMediatorDTO( Mediator mediator) {
		   return MediatorDTO.builder()
				   .name(mediator.getName())
				   .fathersName(mediator.getFathersName()).
				   permanentAddress(mediator.getPermanentAddress()).
				   city(mediator.getCity())
				   .state(mediator.getState()).postalCode(mediator.getPostalCode()).
				   emailId(mediator.getEmailId())
				   .mobileNumber(mediator.getMobileNumber()).
				   alternateMobileNumber(mediator.getAlternateMobileNumber())
				   .creationTt(mediator.getCreationTt())

				   .build();
	}
}
