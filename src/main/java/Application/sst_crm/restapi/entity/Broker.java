package Application.sst_crm.restapi.entity;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.BrokerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Broker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "brokerId")
	Long Id;
	String name;
	String fathersName;
	String permanentAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	
	
	public static BrokerDTO prepareBrokerDTO(Broker broker) {
		   return BrokerDTO.builder()
				   .name(broker.getName())
				   .fathersName(broker.getFathersName()).
				   permanentAddress(broker.getPermanentAddress()).
				   city(broker.getCity())
				   .state(broker.getState()).postalCode(broker.getPostalCode()).
				   emailId(broker.getEmailId())
				   .mobileNumber(broker.getMobileNumber()).
				   alternateMobileNumber(broker.getAlternateMobileNumber())
				   .creationTt(broker.getCreationTt())
				   .build();
	}

}
