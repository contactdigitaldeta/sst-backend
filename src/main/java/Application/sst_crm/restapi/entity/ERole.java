package Application.sst_crm.restapi.entity;

public enum ERole {
	 ROLE_SUPERVISOR,
	    ROLE_ADMIN
}
