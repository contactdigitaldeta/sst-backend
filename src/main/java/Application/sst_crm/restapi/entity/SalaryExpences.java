package Application.sst_crm.restapi.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.SalaryExpencesDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalaryExpences {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SalaryExpenceId")
	Long Id;
	String personName;
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String moodOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static SalaryExpencesDTO prepareSalaryExpencesDTO(SalaryExpences salaryExpences) {
		return SalaryExpencesDTO.builder()
		.personName(salaryExpences.getPersonName())
		.creationTt(salaryExpences.getCreationTt())
		.dateOfPayment(salaryExpences.getDateOfPayment())
		.amount(salaryExpences.getAmount())
		.moodOfPayment(salaryExpences.getMoodOfPayment())
		.purposeOfPayment(salaryExpences.getPurposeOfPayment())
		.referenceNo(salaryExpences.getReferenceNo())
		.build();
	}
}
