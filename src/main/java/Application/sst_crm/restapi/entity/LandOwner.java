package Application.sst_crm.restapi.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.LandOwnerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class LandOwner {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "landownerId")
	
	Long Id;
	String name;
	String fathersName;
	String permanentAddress;
	Integer postalCode;
	String correspondenceAddress;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	String area;
	Long landPlotNumber;
	Long khataNumber;
	Long landTotalCost;
	String monthlyScheme;
	Long advanceRupees;
	@Temporal(TemporalType.DATE)
	Date bookingDate;
	@Temporal(TemporalType.DATE)
	Date agreementDate;
	String introduced;
	String verify;
	String selectDistricts;
	String selectTahasil;
	String selectVillage;
	String riCircle;
	Long khatiyanNumber;
	String brokerName;
	Long totalBrokerCommission;
	Long advancePayment;
	Long dueAmount;
	@Temporal(TemporalType.DATE)
	Date dueDate;
	String occupation;
	
	@OneToMany(targetEntity=LandInstallments.class,mappedBy="landOwnerEntity", cascade = CascadeType.ALL)     
	private List<LandInstallments>landinstallments;
	
	@OneToMany(targetEntity=Brokerinstallments.class,mappedBy="landOwnerEntity", cascade = CascadeType.ALL)     
	private List<Brokerinstallments>brokerinstallments;
	
	
	public void addLandInstallments(LandInstallments landinstallmentslist) {
		
		landinstallments.add(landinstallmentslist);
		
	}
	
	public void addBrokerInstallments(Brokerinstallments brokerinstallmentslist) {
		
		brokerinstallments.add(brokerinstallmentslist);
	}
	
	public static LandOwnerDTO prepareLandOwnerDTO(LandOwner landOwner) {
		   return LandOwnerDTO.builder()
				   .brokerinstallmentslist(landOwner.getBrokerinstallments())
				   .landinstallmentslist(landOwner.getLandinstallments())
				   .name(landOwner.getName())
				   .fathersName(landOwner.getFathersName()).
				   permanentAddress(landOwner.getPermanentAddress()).
                    postalCode(landOwner.getPostalCode()).
				   correspondenceAddress(landOwner.getCorrespondenceAddress()).
				   emailId(landOwner.getEmailId())
				   .mobileNumber(landOwner.getMobileNumber()).
				   alternateMobileNumber(landOwner.getAlternateMobileNumber()).
				   dateOfBirth(landOwner.getDateOfBirth())
				   .creationTt(landOwner.getCreationTt())
				   .area(landOwner.getArea())
				   .landPlotNumber(landOwner.getLandPlotNumber())
				   .khataNumber(landOwner.getKhataNumber())
				   .landTotalCost(landOwner.getLandTotalCost())
				   .monthlyScheme(landOwner.getMonthlyScheme())
				   .advanceRupees(landOwner.getAdvanceRupees())
				   .bookingDate(landOwner.getBookingDate())
				   .agreementDate(landOwner.getAgreementDate())
				   .introduced(landOwner.getIntroduced())
				   .verify(landOwner.getVerify())
				   .selectDistricts(landOwner.getSelectDistricts())
				   .selectTahasil(landOwner.getSelectTahasil())
				   .selectVillage(landOwner.getSelectVillage())
				   .riCircle(landOwner.getRiCircle())
				   .khatiyanNumber(landOwner.getKhatiyanNumber())
				   .brokerName(landOwner.getBrokerName())
				   .totalBrokerCommission(landOwner.getTotalBrokerCommission())
				   .advancePayment(landOwner.getAdvancePayment())
				   .dueAmount(landOwner.getDueAmount())
				   .dueDate(landOwner.getDueDate())
				   .occupation(landOwner.getOccupation())
				   	.build();
		   
	}
				   
				   
				   
}
