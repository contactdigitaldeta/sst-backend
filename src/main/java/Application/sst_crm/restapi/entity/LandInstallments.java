package Application.sst_crm.restapi.entity;


import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import Application.sst_crm.restapi.dto.LandInstallmentsDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LandInstallments {
	
	    @javax.persistence.Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "LandInstallmentId")
	    Long Id;
	    @Temporal(TemporalType.DATE)
	    Date date;
	    Long amount;
	    String modeOfPayment;
	    Long chequeNo;
	    
	    @JsonIgnore
	    @ManyToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name="landownerId")
	    LandOwner landOwnerEntity;
	    
	    public static LandInstallmentsDTO prepareInstallmentsDTO(LandInstallments landinstallments) {
	    	
	    	return LandInstallmentsDTO.builder().date(landinstallments.getDate())
	    			.amount(landinstallments.getAmount())
	    			.modeOfPayment(landinstallments.getModeOfPayment())
	    			.chequeNo(landinstallments.getChequeNo())
	    			.build();
	    }
	  
}
