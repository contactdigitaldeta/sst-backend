package Application.sst_crm.restapi.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customerId")
	Long Id;
	String firstName;
	String middleName;
	String lastName;
	String fathersName;
	String permanentAddress;
	String correspondenceAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	String occupation;
	@Temporal(TemporalType.DATE)
	Date dateOfBooking;
	
	public static CustomerDTO prepareCustomerDTO(Customer customer) {
		   return CustomerDTO.builder()
				   .firstName(customer.getFirstName())
				   .middleName(customer.getMiddleName())
				   .lastName(customer.getLastName())
				   .fathersName(customer.getFathersName()).
				   permanentAddress(customer.getPermanentAddress())
				   .correspondenceAddress(customer.getCorrespondenceAddress()).
				   city(customer.getCity())
				   .state(customer.getState()).postalCode(customer.getPostalCode()).
				   emailId(customer.getEmailId())
				   .mobileNumber(customer.getMobileNumber()).
				   alternateMobileNumber(customer.getAlternateMobileNumber()).
				   dateOfBirth(customer.getDateOfBirth())
				   .creationTt(customer.getCreationTt())
				   .occupation(customer.getOccupation())
				   .dateOfBooking(customer.getDateOfBooking()).build();
	}


}
