package Application.sst_crm.restapi.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.CourtExpencesDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourtExpences {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "courtExpenceId")
	Long Id;
	String personName;
	@CreationTimestamp
	@Temporal(TemporalType.DATE)	
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String moodOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static CourtExpencesDTO prepareCourtExpencesDTO(CourtExpences courtExpences) {
		return CourtExpencesDTO.builder()
		.personName(courtExpences.getPersonName())
		.creationTt(courtExpences.getCreationTt())
		.dateOfPayment(courtExpences.getDateOfPayment())
		.amount(courtExpences.getAmount())
		.moodOfPayment(courtExpences.getMoodOfPayment())
		.purposeOfPayment(courtExpences.getPurposeOfPayment())
		.referenceNo(courtExpences.getReferenceNo())
		.build();
	}
}
