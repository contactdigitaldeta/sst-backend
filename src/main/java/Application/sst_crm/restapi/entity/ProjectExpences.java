package Application.sst_crm.restapi.entity;

import java.util.Date;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.ProjectExpencesDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectExpences {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "projectExpenceId")
	Long Id;
	String projectName;
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfStart;
	String location;
	
	
	@OneToMany(targetEntity = PexpenseSlot.class, mappedBy="ProjectExpenseEntity", cascade = CascadeType.ALL)
	private List<PexpenseSlot> pexpenseslots;
	
	public void addPexpenseSlot(PexpenseSlot pexpenseslotlist) {
		pexpenseslots.add(pexpenseslotlist);
	}
	
	public static ProjectExpencesDTO prepareProjectExpencesDTO(ProjectExpences projectExpences) {
		return ProjectExpencesDTO.builder()
		.projectName(projectExpences.getProjectName())
		.location(projectExpences.getLocation())
		.pexpenseslotslist(projectExpences.getPexpenseslots())
		.creationTt(projectExpences.getCreationTt())
		.dateOfStart(projectExpences.getDateOfStart())
		.build();
	}
}
