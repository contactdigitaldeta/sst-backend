package Application.sst_crm.restapi.entity;

import java.util.Date;


import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.dto.BookingDTO;

@Entity()
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Booking {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bookingId")
	Long Id;
	String firstName;
	String middleName;
	String lastName;
	String fathersName;
	String permanentAddress;
	String correspondenceAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	String occupation;
	String mediatorName;
	Long commissionAmount;
	String reference;
	String schemeName;
	Integer optionForMoodOfPayment;
	Integer cornerPlot;
	Long totalCostRS;
	Long advanceRS;
	Long monthlyScheme;
	Long fullPayment;
	Long moneyReceiptNumber;
	Long plotSize;
	@Temporal(TemporalType.DATE)
	Date bookingDate;
	@Temporal(TemporalType.DATE)
	Date agreementDate;
	String introduced;
	String verifiedBy;
	

	@OneToMany(targetEntity = Installments.class, mappedBy="bookingEntity", cascade = CascadeType.ALL)
	private List<Installments> installments;
	
	public void addInstallments(Installments installmentList) {
		installments.add(installmentList);
	}
	
	public static BookingDTO prepareBookingDTO(Booking booking) {
		   return BookingDTO.builder()
				   .installmentsList(booking.getInstallments())
				   .firstName(booking.getFirstName())
				   .middleName(booking.getMiddleName())
				   .lastName(booking.getLastName())
				   .fathersName(booking.getFathersName()).
				   permanentAddress(booking.getPermanentAddress())
				   .correspondenceAddress(booking.getCorrespondenceAddress()).
				   city(booking.getCity())
				   .state(booking.getState()).postalCode(booking.getPostalCode()).
				   emailId(booking.getEmailId())
				   .mobileNumber(booking.getMobileNumber()).
				   alternateMobileNumber(booking.getAlternateMobileNumber()).
				   dateOfBirth(booking.getDateOfBirth())
				   .creationTt(booking.getCreationTt())
				   .occupation(booking.getOccupation()).
				   mediatorName(booking.getMediatorName())
				   .commissionAmount(booking.getCommissionAmount())
				   .reference(booking.getReference())
				   .schemeName(booking.getSchemeName()).
				   optionForMoodOfPayment(booking.getOptionForMoodOfPayment())
				   .cornerPlot(booking.getCornerPlot()).
				   totalCostRS(booking.getTotalCostRS())
				   .advanceRS(booking.getAdvanceRS())
				   .monthlyScheme(booking.getMonthlyScheme()).
				   fullPayment(booking.getFullPayment())
				   .moneyReceiptNumber(booking.getMoneyReceiptNumber()).
				   plotSize(booking.getPlotSize())
				   .bookingDate(booking.getBookingDate())
				   .agreementDate(booking.getAgreementDate())
				   .introduced(booking.getIntroduced())
				   .verifiedBy(booking.getVerifiedBy()).build();
	}
	

}
		  
	

				   
				   
				   