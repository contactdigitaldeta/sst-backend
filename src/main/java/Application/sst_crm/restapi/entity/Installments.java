package Application.sst_crm.restapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import Application.sst_crm.restapi.dto.InstallmentsDTO;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Installments {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "installmentId")
    Long Id;
    @Temporal(TemporalType.DATE)
    Date date;
    Long amount;
    String modeOfPayment;
    Long chequeNo;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="bookingId")
    Booking bookingEntity;
    
public static InstallmentsDTO prepareInstallmentsDTO(Installments installments) {
    	
    	return InstallmentsDTO.builder().date(installments.getDate())
    			.amount(installments.getAmount())
    			.modeOfPayment(installments.getModeOfPayment())
    			.chequeNo(installments.getChequeNo())
    			.build();
    	
    }

}
