package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.CourtExpences;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourtExpencesDTO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "courtExpenceId")
	Long Id;
	String personName;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String moodOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static CourtExpences prepareCourtExpencesEntity(CourtExpencesDTO courtExpencesDTO) {
		return CourtExpences.builder()
				.personName(courtExpencesDTO.getPersonName())
				.creationTt(courtExpencesDTO.getCreationTt())
				.dateOfPayment(courtExpencesDTO.getDateOfPayment())
				.amount(courtExpencesDTO.getAmount())
				.moodOfPayment(courtExpencesDTO.getMoodOfPayment())
				.purposeOfPayment(courtExpencesDTO.getPurposeOfPayment())
				.referenceNo(courtExpencesDTO.getReferenceNo())
				.build();
				
	}
}
