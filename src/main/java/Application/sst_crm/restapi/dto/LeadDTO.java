package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Column;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Lead;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class LeadDTO {
	Integer leadType;
	String firstName;
	String middleName;
	String lastName;
	Long phoneNumber;
	String address;
	String city;
	String state;
	Integer pinCode;
	String companyName;
	String designation;
	String source;
	String assignTo;
	String status;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	public static Lead prepareLeadEntity(LeadDTO leadDTO) {
		return Lead.builder()
				  .leadType(leadDTO.getLeadType()).firstName(leadDTO.getFirstName()).middleName(leadDTO.getMiddleName()).
				   lastName(leadDTO.getLastName()).phoneNumber(leadDTO.getPhoneNumber()).
				   address(leadDTO.getAddress()).
				   city(leadDTO.getCity()).state(leadDTO.getState()).pinCode(leadDTO.getPinCode()).
				   companyName(leadDTO.getCompanyName()).designation(leadDTO.getDesignation()).source(leadDTO.getSource()).
				   assignTo(leadDTO.getAssignTo()).status(leadDTO.getStatus()).creationTt(leadDTO.getCreationTt()).build();

}
}
