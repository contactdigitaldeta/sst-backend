package Application.sst_crm.restapi.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Brokerinstallments;
import Application.sst_crm.restapi.entity.LandInstallments;
import Application.sst_crm.restapi.entity.LandOwner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class LandOwnerDTO {
	String name;
	String fathersName;
	String permanentAddress;
	Integer postalCode;
	String correspondenceAddress;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	String area;
	Long landPlotNumber;
	Long khataNumber;
	Long landTotalCost;
	String monthlyScheme;
	Long advanceRupees;
	@Temporal(TemporalType.DATE)
	Date bookingDate;
	@Temporal(TemporalType.DATE)
	Date agreementDate;
	String introduced;
	String verify;
	String selectDistricts;
	String selectTahasil;
	String selectVillage;
	String riCircle;
	Long khatiyanNumber;
	String brokerName;
	Long totalBrokerCommission;
	Long advancePayment;
	Long dueAmount;
	@Temporal(TemporalType.DATE)
	Date dueDate;
	String occupation;
	List<LandInstallments> landinstallmentslist;
	List<Brokerinstallments> brokerinstallmentslist;
	
	
	
	public static LandOwner prepareLandOwnerEntity(LandOwnerDTO landOwnerDTO) {
		   return LandOwner.builder()
				   .name(landOwnerDTO.getName())
				   .fathersName(landOwnerDTO.getFathersName()).
				   permanentAddress(landOwnerDTO.getPermanentAddress()).
                 postalCode(landOwnerDTO.getPostalCode()).
				   correspondenceAddress(landOwnerDTO.getCorrespondenceAddress()).
				   emailId(landOwnerDTO.getEmailId())
				   .mobileNumber(landOwnerDTO.getMobileNumber()).
				   alternateMobileNumber(landOwnerDTO.getAlternateMobileNumber()).
				   dateOfBirth(landOwnerDTO.getDateOfBirth())
				   
				   .area(landOwnerDTO.getArea())
				   .landPlotNumber(landOwnerDTO.getLandPlotNumber())
				   .khataNumber(landOwnerDTO.getKhataNumber())
				   .landTotalCost(landOwnerDTO.getLandTotalCost())
				   .monthlyScheme(landOwnerDTO.getMonthlyScheme())
				   .advanceRupees(landOwnerDTO.getAdvanceRupees())
				   .bookingDate(landOwnerDTO.getBookingDate())
				   .agreementDate(landOwnerDTO.getAgreementDate())
				   .introduced(landOwnerDTO.getIntroduced())
				   .verify(landOwnerDTO.getVerify())
				   .selectDistricts(landOwnerDTO.getSelectDistricts())
				   .selectTahasil(landOwnerDTO.getSelectTahasil())
				   .selectVillage(landOwnerDTO.getSelectVillage())
				   .riCircle(landOwnerDTO.getRiCircle())
				   .khatiyanNumber(landOwnerDTO.getKhatiyanNumber())
				   .brokerName(landOwnerDTO.getBrokerName())
				   .totalBrokerCommission(landOwnerDTO.getTotalBrokerCommission())
				   .advancePayment(landOwnerDTO.getAdvancePayment())
				   .dueAmount(landOwnerDTO.getDueAmount())
				   .occupation(landOwnerDTO.getOccupation())
				   .dueDate(landOwnerDTO.getDueDate())
				   .occupation(landOwnerDTO.getOccupation())
				   .landinstallments(landOwnerDTO.getLandinstallmentslist())
				   .brokerinstallments(landOwnerDTO.getBrokerinstallmentslist())
				   	.build();
	}
				   
				   
				   
}
