package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Mediator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class MediatorDTO {
	
	String name;
	String fathersName;
	String permanentAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	public static Mediator prepareMediatorEntity(MediatorDTO mediatorDTO) {
		return Mediator.builder()
				  .name(mediatorDTO.getName()).fathersName(mediatorDTO.getFathersName()).
				   permanentAddress(mediatorDTO.getPermanentAddress()).
				   city(mediatorDTO.getCity()).state(mediatorDTO.getState()).postalCode(mediatorDTO.getPostalCode()).
				   emailId(mediatorDTO.getEmailId()).mobileNumber(mediatorDTO.getMobileNumber()).
				   alternateMobileNumber(mediatorDTO.getAlternateMobileNumber())
				   

				   .build();
	}

}
