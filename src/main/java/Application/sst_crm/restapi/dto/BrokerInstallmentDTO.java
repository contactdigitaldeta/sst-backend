package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import Application.sst_crm.restapi.entity.Brokerinstallments;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BrokerInstallmentDTO {
	
	Long Id;
	@Temporal(TemporalType.DATE)
	Date date;
	Long amount;
	String modeOfPayment;
	Long chequeNo;
	
	
	public static Brokerinstallments prepareBrokerinstallmentsEntity(BrokerInstallmentDTO brokerInstallmentDTO) {
		
		
		return Brokerinstallments.builder().date(brokerInstallmentDTO.getDate()).amount(brokerInstallmentDTO.getAmount())
				.modeOfPayment(brokerInstallmentDTO.getModeOfPayment()).chequeNo(brokerInstallmentDTO.getChequeNo())
				.build();
	}
	
	

}
