package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import Application.sst_crm.restapi.entity.PexpenseSlot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PexpenseSlotDTO {
	
	Long Id;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String modeOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static PexpenseSlot prepareProjectExpences(PexpenseSlotDTO pexpenseslotDto) {
		
		return PexpenseSlot.builder().dateOfPayment(pexpenseslotDto.getDateOfPayment())
				.amount(pexpenseslotDto.getAmount())
				.modeOfPayment(pexpenseslotDto.getModeOfPayment())
				.purposeOfPayment(pexpenseslotDto.getPurposeOfPayment())
				.referenceNo(pexpenseslotDto.getReferenceNo()).build();
				
	}
	
	
	

}
