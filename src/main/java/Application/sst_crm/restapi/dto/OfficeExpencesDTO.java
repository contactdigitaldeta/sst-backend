package Application.sst_crm.restapi.dto;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.OfficeExpences;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OfficeExpencesDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "officeExpenceId")
	Long Id;
	String personName;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String moodOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static OfficeExpences prepareOfficeExpencesEntity(OfficeExpencesDTO officeExpencesDTO) {
		return OfficeExpences.builder()
				.personName(officeExpencesDTO.getPersonName())
				.creationTt(officeExpencesDTO.getCreationTt())
				.dateOfPayment(officeExpencesDTO.getDateOfPayment())
				.amount(officeExpencesDTO.getAmount())
				.moodOfPayment(officeExpencesDTO.getMoodOfPayment())
				.purposeOfPayment(officeExpencesDTO.getPurposeOfPayment())
				.referenceNo(officeExpencesDTO.getReferenceNo())
				.build();
				
	}
}
