package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import Application.sst_crm.restapi.entity.LandInstallments;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LandInstallmentsDTO {
	
	
	Long Id;
	@Temporal(TemporalType.DATE)
	Date date;
	Long amount;
	String modeOfPayment;
	Long chequeNo;
	
	
	public static LandInstallments prepareInstallmentsEntity(LandInstallmentsDTO landinstallmentDto) {

		return LandInstallments.builder().date(landinstallmentDto.getDate()).amount(landinstallmentDto.getAmount())
				.modeOfPayment(landinstallmentDto.getModeOfPayment()).chequeNo(landinstallmentDto.getChequeNo())
				.build();

	}
	
	
	

}
