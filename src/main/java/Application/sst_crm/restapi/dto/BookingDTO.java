package Application.sst_crm.restapi.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Booking;
import Application.sst_crm.restapi.entity.Installments;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BookingDTO {
	String firstName;
	String middleName;
	String lastName;
	String fathersName;
	String permanentAddress;
	String correspondenceAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "creation_tt", nullable = false, updatable = false)
	Date creationTt;
	String occupation;
	String mediatorName;
	Long commissionAmount;
	String reference;
	String schemeName;
	Integer optionForMoodOfPayment;
	Integer cornerPlot;
	Long totalCostRS;
	Long advanceRS;
	Long monthlyScheme;
	Long fullPayment;
	Long moneyReceiptNumber;
	Long plotSize;
	@Temporal(TemporalType.DATE)
	Date bookingDate;
	@Temporal(TemporalType.DATE)
	Date agreementDate;
	String introduced;
	String verifiedBy;
	List<Installments> installmentsList;
	

	public static Booking prepareBookingEntity(BookingDTO bookingDTO) {

		return Booking.builder().firstName(bookingDTO.getFirstName()).middleName(bookingDTO.getMiddleName())
				.lastName(bookingDTO.getLastName()).fathersName(bookingDTO.getFathersName())
				.permanentAddress(bookingDTO.getPermanentAddress())
				.correspondenceAddress(bookingDTO.getCorrespondenceAddress()).city(bookingDTO.getCity())
				.state(bookingDTO.getState()).postalCode(bookingDTO.getPostalCode()).emailId(bookingDTO.getEmailId())
				.mobileNumber(bookingDTO.getMobileNumber()).alternateMobileNumber(bookingDTO.getAlternateMobileNumber())
				.dateOfBirth(bookingDTO.getDateOfBirth()).occupation(bookingDTO.getOccupation())
				.mediatorName(bookingDTO.getMediatorName()).commissionAmount(bookingDTO.getCommissionAmount())
				.reference(bookingDTO.getReference()).schemeName(bookingDTO.getSchemeName())
				.optionForMoodOfPayment(bookingDTO.getOptionForMoodOfPayment()).cornerPlot(bookingDTO.getCornerPlot())
				.totalCostRS(bookingDTO.getTotalCostRS()).advanceRS(bookingDTO.getAdvanceRS())
				.monthlyScheme(bookingDTO.getMonthlyScheme()).fullPayment(bookingDTO.getFullPayment())
				.moneyReceiptNumber(bookingDTO.getMoneyReceiptNumber()).plotSize(bookingDTO.getPlotSize())
				.bookingDate(bookingDTO.getBookingDate()).agreementDate(bookingDTO.getAgreementDate())
				.introduced(bookingDTO.getIntroduced()).verifiedBy(bookingDTO.getVerifiedBy())
				.installments(bookingDTO.getInstallmentsList()).build();

	}
}
