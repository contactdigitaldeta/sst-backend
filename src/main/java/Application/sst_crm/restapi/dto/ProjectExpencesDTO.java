package Application.sst_crm.restapi.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.PexpenseSlot;
import Application.sst_crm.restapi.entity.ProjectExpences;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectExpencesDTO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "projectExpenceId")
	Long Id;
	String projectName;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfStart;
	String location;
	List<PexpenseSlot> pexpenseslotslist;
	
	public static ProjectExpences prepareProjectExpencesEntity(ProjectExpencesDTO projectExpencesDTO) {
		return ProjectExpences.builder()
				.projectName(projectExpencesDTO.getProjectName())
				.creationTt(projectExpencesDTO.getCreationTt())
				.dateOfStart(projectExpencesDTO.getDateOfStart())
				.pexpenseslots(projectExpencesDTO.getPexpenseslotslist())
				.location(projectExpencesDTO.getLocation())
				.build();
				
	}
}
