package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import Application.sst_crm.restapi.entity.Installments;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InstallmentsDTO {

	Long Id;
	@Temporal(TemporalType.DATE)
	Date date;
	Long amount;
	String modeOfPayment;
	Long chequeNo;

	public static Installments prepareInstallmentsEntity(InstallmentsDTO installmentDto) {

		return Installments.builder().date(installmentDto.getDate()).amount(installmentDto.getAmount())
				.modeOfPayment(installmentDto.getModeOfPayment()).chequeNo(installmentDto.getChequeNo())
				.build();

	}

}
