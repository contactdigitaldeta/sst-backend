package Application.sst_crm.restapi.dto;



import java.util.Date;

import javax.persistence.Column;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Broker;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class BrokerDTO {
	
	String name;
	String fathersName;
	String permanentAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	
	
	
	public static Broker prepareBrokerEntity(BrokerDTO brokerDTO) {
		return Broker.builder()
				  .name(brokerDTO.getName()).fathersName(brokerDTO.getFathersName()).
				   permanentAddress(brokerDTO.getPermanentAddress()).
				   city(brokerDTO.getCity()).state(brokerDTO.getState()).postalCode(brokerDTO.getPostalCode()).
				   emailId(brokerDTO.getEmailId()).mobileNumber(brokerDTO.getMobileNumber()).
				   alternateMobileNumber(brokerDTO.getAlternateMobileNumber()).
				   build();
	}


}
