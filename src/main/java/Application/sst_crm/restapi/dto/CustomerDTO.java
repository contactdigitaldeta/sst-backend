package Application.sst_crm.restapi.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CustomerDTO {
	
	String firstName;
	String middleName;
	String lastName;
	String fathersName;
	String permanentAddress;
	String correspondenceAddress;
	String city;
	String state;
	Integer postalCode;
	String emailId;
	Long mobileNumber;
	Long alternateMobileNumber;
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	String occupation;
	@Temporal(TemporalType.DATE)
	Date dateOfBooking;
	
	
	public static Customer prepareCustomerEntity(CustomerDTO customerDTO) {
		return Customer.builder()
				  .firstName(customerDTO.getFirstName()).middleName(customerDTO.getMiddleName()).
				   lastName(customerDTO.getLastName()).fathersName(customerDTO.getFathersName()).
				   permanentAddress(customerDTO.getPermanentAddress()).correspondenceAddress(customerDTO.getCorrespondenceAddress()).
				   city(customerDTO.getCity()).state(customerDTO.getState()).postalCode(customerDTO.getPostalCode()).
				   emailId(customerDTO.getEmailId()).mobileNumber(customerDTO.getMobileNumber()).
				   alternateMobileNumber(customerDTO.getAlternateMobileNumber()).dateOfBirth(customerDTO.getDateOfBirth()).
				   occupation(customerDTO.getOccupation()).dateOfBooking(customerDTO.getDateOfBooking())
				   
.build();
	}


	
}
