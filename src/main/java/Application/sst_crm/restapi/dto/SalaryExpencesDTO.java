package Application.sst_crm.restapi.dto;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import Application.sst_crm.restapi.entity.SalaryExpences;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalaryExpencesDTO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "projectExpenceId")
	Long Id;
	String personName;
	@CreationTimestamp
	@Column(name="creation_tt" ,nullable=false,updatable=false)
	Date creationTt;
	@Temporal(TemporalType.DATE)
	Date dateOfPayment;
	Long amount;
	String moodOfPayment;
	String purposeOfPayment;
	Long referenceNo;
	
	public static SalaryExpences prepareSalaryExpencesEntity(SalaryExpencesDTO salaryExpencesDTO) {
		return SalaryExpences.builder()
				.personName(salaryExpencesDTO.getPersonName())
				.creationTt(salaryExpencesDTO.getCreationTt())
				.dateOfPayment(salaryExpencesDTO.getDateOfPayment())
				.amount(salaryExpencesDTO.getAmount())
				.moodOfPayment(salaryExpencesDTO.getMoodOfPayment())
				.purposeOfPayment(salaryExpencesDTO.getPurposeOfPayment())
				.referenceNo(salaryExpencesDTO.getReferenceNo())
				.build();
				
	}
}
