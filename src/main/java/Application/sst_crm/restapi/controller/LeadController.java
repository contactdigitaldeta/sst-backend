package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.LeadDTO;
import Application.sst_crm.restapi.entity.Lead;
import Application.sst_crm.restapi.service.LeadService;
@CrossOrigin(origins="*")

@RestController
@RequestMapping("/leads")

public class LeadController {
	
	@Autowired
	
	private LeadService leadService;
	
	@GetMapping(produces="application/json")
	
	public List<Lead> getLeads(){
		return leadService.getLeads();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<Lead> getLeadsById(@PathVariable("id") Long id){
		return leadService.getLeadById(id);
	}
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<String>createLead(@RequestBody LeadDTO lead)
	{
		String response = leadService.insertLead(lead);
		return  ResponseEntity.ok(response);
	}
	
	@PutMapping("/{id}")
	
	public String updateLead(@RequestBody LeadDTO lead, @PathVariable("id") Long id) {
		System.out.println("id is"+id);
		return leadService.updateLead(lead,id);
	}
	
	@DeleteMapping("/{id}")
	public String deleteLead(@PathVariable("id") Long id)
	{
		return leadService.deleteLead(id);
	}

}
