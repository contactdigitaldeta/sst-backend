package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.MediatorDTO;
import Application.sst_crm.restapi.entity.Mediator;
import Application.sst_crm.restapi.service.MediatorService;

@CrossOrigin(origins="*")

@RestController
@RequestMapping("/mediators")

public class MediatorController {

	@Autowired
	private MediatorService mediatorService;
	@GetMapping(produces="application/json")
	public List<Mediator> getMediators() {
		
		return mediatorService.getMediators();
	}
		@GetMapping(value = "/{id}", produces = "application/json")
		public Optional<Mediator> getMediatorsById(@PathVariable("id") Long id){
			
			return mediatorService.getMediatorById(id);
		}
		
		@PostMapping(consumes="application/json")
		public ResponseEntity<String>createMediator(@RequestBody MediatorDTO mediator)
		{
			String response = mediatorService.insertMediator(mediator);
			return ResponseEntity.ok(response);
			
		}
		
		@PutMapping("/{id}")
		public String updateMediator(@RequestBody MediatorDTO mediator, @PathVariable("id") Long id) {
			System.out.println("id is"+id);
			return mediatorService.updateMediator(mediator,id);
			
		}
		
		@DeleteMapping("/{id}")
		public String deleteMediator(@PathVariable("id") Long id )
		{
			return mediatorService.deleteMediator(id);
		}
}
