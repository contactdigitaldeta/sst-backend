package Application.sst_crm.restapi.controller;

import java.util.List;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.CourtExpencesDTO;
import Application.sst_crm.restapi.entity.CourtExpences;

import Application.sst_crm.restapi.service.CourtExpencesService;


//@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/courtExpenses")
public class CourtExpencesController {
	@Autowired
	private CourtExpencesService  courtExpencesService;
	@GetMapping(produces="application/json")
	public List<CourtExpences> getcourtExpences() {
		
		return courtExpencesService.getCourtExpences();
		
	}
	

	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<CourtExpences> getCourtExpencesById(@PathVariable("id") Long id){
		
		return courtExpencesService.getCourtExpencesById(id);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<String>createCourtExpences(@RequestBody CourtExpencesDTO courtExpences)
	{
		String response = courtExpencesService.insertCourtExpences(courtExpences);
		return ResponseEntity.ok(response);
		
	}
	
	@PutMapping("/{id}")
	public String updateCourtExpences(@RequestBody CourtExpencesDTO courtExpences, @PathVariable("id") Long id) {
		System.out.println("id is"+id);
		return courtExpencesService.updateCourtExpences(courtExpences,id);
		
	}
	@DeleteMapping("/{id}")
	public String deleteCourtExpences(@PathVariable("id") Long id )
	{
		return courtExpencesService.deleteCourtExpences(id);
	}
}
