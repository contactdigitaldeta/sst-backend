package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.LandOwnerDTO;
import Application.sst_crm.restapi.entity.LandOwner;
import Application.sst_crm.restapi.service.LandOwnerService;

@CrossOrigin(origins="*")

@RestController
@RequestMapping("/landowners")

public class LandOwnerController {

	@Autowired
	private LandOwnerService landownerService;
	@GetMapping(produces="application/json")
	public List<LandOwner> getLandOwners() {
		
		return landownerService.getLandOwners();
	}
		@GetMapping(value = "/{id}", produces = "application/json")
		public Optional<LandOwner> getLandOwnersById(@PathVariable("id") Long id){
			
			return landownerService.getLandOwnerById(id);
		}
		
		@PostMapping(consumes="application/json")
		public ResponseEntity<String>createLandOwner(@RequestBody LandOwnerDTO landowner)
		{
			String response = landownerService.insertLandOwner(landowner);
			return ResponseEntity.ok(response);
			
		}
		
		@PutMapping("/{id}")
		public String updateLandOwner(@RequestBody LandOwnerDTO landowner, @PathVariable("id") Long id) {
			System.out.println("id is"+id);
			return landownerService.updateLandOwner(landowner,id);
			
		}
		
		@DeleteMapping("/{id}")
		public String deleteLandOwner(@PathVariable("id") Long id )
		{
			return landownerService.deleteLandOwner(id);
		}
}
