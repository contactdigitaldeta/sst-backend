package Application.sst_crm.restapi.controller;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.service.MediatorService;


@RestController
@RequestMapping("/All")

public class MediatorDropdownController {
	
	@Autowired
	private MediatorService mediatorservice;
	
	@GetMapping(value="/mediatorcount", produces="application/json")
	public List<String> getallmediators(){
		List<String> listOfallmediators = mediatorservice.getAllmediator();
		listOfallmediators.sort(Comparator.naturalOrder());
		return listOfallmediators;
	}

}
