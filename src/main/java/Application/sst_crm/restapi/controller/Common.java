package Application.sst_crm.restapi.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import Application.sst_crm.restapi.service.BookingService;
import Application.sst_crm.restapi.service.LeadService;
import Application.sst_crm.restapi.service.CustomerService;
import Application.sst_crm.restapi.service.LandOwnerService;
import Application.sst_crm.restapi.service.MediatorService;
import Application.sst_crm.restapi.service.BrokerService;
@RestController
@RequestMapping("common")
public class Common {
	
	@Autowired
	private BookingService BookingService;
	
	@Autowired
	private LeadService leadService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LandOwnerService landOwnerService;
	
	@Autowired
	private BrokerService BrokerService;
	
	@Autowired
	private MediatorService MediatorService;
	
	@GetMapping(value = "/Allbookings", produces = "application/json")
	 public Long getAllCount(){
		 Long allbookingsCount = BookingService.getBookingsCount();
		 return allbookingsCount;
		  
	 }
	@GetMapping(value = "/Allleads", produces = "application/json")
	 public Long getAllLEADSCount(){
		 Long allLeadsCount = leadService.getLeadsCount();
		 return allLeadsCount;
		  
	 }
	 @GetMapping(value = "/Allcustomers", produces = "application/json")
	 public Long getAllcustomerCount(){
		 Long allCustomersCount = customerService.getCustomerCount();
		 return allCustomersCount;
		
		  
	 }
	 @GetMapping(value = "/Alllandowners", produces = "application/json")
	 public Long getAlllandOwnerCount(){
		 Long alllandOwnersCount = landOwnerService.getLandOwnerCount();
		 return alllandOwnersCount;
		  
	 }
	 @GetMapping(value = "/Allbrokers", produces = "application/json")
	 public Long getAllBrokerCount(){
		 Long allbrokersCount = BrokerService.getBrokerCount();
		 return allbrokersCount;
		  
	 }
	 
	 @GetMapping(value = "/Allmediators", produces = "application/json")
	 public Long getAllMediatorCount(){
		 Long allmediatorsCount = MediatorService.getMediatorCount();
		 return allmediatorsCount;
		  
	 }
	  
	
	 }
