package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.BookingDTO;
import Application.sst_crm.restapi.entity.Booking;
import Application.sst_crm.restapi.service.BookingService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/bookings")
public class BookingController {

	Logger logger = LoggerFactory.getLogger(BookingController.class);

	@Autowired
	private BookingService bookingService;

	@GetMapping(produces = "application/json")
	public List<Booking> getBoookings() {

		return bookingService.getBookings();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<Booking> getBookingsById(@PathVariable("id") Long id) {

		return bookingService.getBookingById(id);
	}

	@PostMapping(consumes = "application/json")
	public ResponseEntity<String> createBooking(@RequestBody BookingDTO booking) {
		logger.info(booking.toString());
		String response = bookingService.insertBooking(booking);
		return ResponseEntity.ok(response);

	}

	@PutMapping("/{id}")
	public String updateBooking(@RequestBody BookingDTO booking, @PathVariable("id") Long id) {
		System.out.println("id is" + id);
		return bookingService.updateBooking(booking, id);

	}

	@DeleteMapping("/{id}")
	public String deleteBooking(@PathVariable("id") Long id) {
		return bookingService.deleteBooking(id);
	}
}
