package Application.sst_crm.restapi.controller;

import java.util.Comparator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.service.BrokerService;

@RestController
@RequestMapping("/All")

public class BrokerDropdownController {
	
	
	
	@Autowired
	private BrokerService brokerservice;
	
	@GetMapping(value="/brokercount", produces="application/json")
	public List<String> getallbrokers(){
		List<String> listOfallbrokers = brokerservice.getAllbroker();
		listOfallbrokers.sort(Comparator.naturalOrder());
		return listOfallbrokers;
	}
	
	
	
}
