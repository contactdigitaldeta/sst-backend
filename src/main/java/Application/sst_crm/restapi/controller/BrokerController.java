package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.BrokerDTO;
import Application.sst_crm.restapi.entity.Broker;
import Application.sst_crm.restapi.service.BrokerService;

@CrossOrigin(origins="*")

@RestController
@RequestMapping("/brokers")

public class BrokerController {

	@Autowired
	private BrokerService brokerService;
	
	
	
	@GetMapping(produces="application/json")
	public List<Broker> getBrokers() {
		
		return brokerService.getBrokers();
	}
		@GetMapping(value = "/{id}", produces = "application/json")
		public Optional<Broker> getBrokersById(@PathVariable("id") Long id){
			
			return brokerService.getBrokerById(id);
		}
		
		@PostMapping(consumes="application/json")
		public ResponseEntity<String>createBroker(@RequestBody BrokerDTO broker)
		{
			String response = brokerService.insertBroker(broker);
			return ResponseEntity.ok(response);
			
		}
		
		@PutMapping("/{id}")
		public String updateBroker(@RequestBody BrokerDTO booking, @PathVariable("id") Long id) {
			System.out.println("id is"+id);
			return brokerService.updateBroker(booking,id);
			
		}
		
		@DeleteMapping("/{id}")
		public String deleteBroker(@PathVariable("id") Long id )
		{
			return brokerService.deleteBroker(id);
		}
}
