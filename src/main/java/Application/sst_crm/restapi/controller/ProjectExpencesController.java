package Application.sst_crm.restapi.controller;

import java.util.List;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import Application.sst_crm.restapi.dto.ProjectExpencesDTO;
import Application.sst_crm.restapi.entity.ProjectExpences;
import Application.sst_crm.restapi.service.ProjectExpencesService;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/projectExpences")
public class ProjectExpencesController {
	
	@Autowired
	private ProjectExpencesService  projectExpencesService;
	@GetMapping(produces="application/json")
	public List<ProjectExpences> getprojectExpences() {
		
		return projectExpencesService.getProjectExpences();
		
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<ProjectExpences> getProjectExpencesById(@PathVariable("id") Long id){
		
		return projectExpencesService.getProjectExpencesById(id);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<String>createProjectExpences(@RequestBody ProjectExpencesDTO projectExpences)
	{
		String response = projectExpencesService.insertProjectExpences(projectExpences);
		return ResponseEntity.ok(response);
		
	}
	
	@PutMapping("/{id}")
	public String updateProjectExpences(@RequestBody ProjectExpencesDTO projectExpences, @PathVariable("id") Long id) {
		System.out.println("id is"+id);
		return projectExpencesService.updateProjectExpences(projectExpences,id);
		
	}
	@DeleteMapping("/{id}")
	public String deleteProjectExpences(@PathVariable("id") Long id )
	{
		return projectExpencesService.deleteProjectExpences(id);
	}
	
	
}
