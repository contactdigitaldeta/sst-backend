package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import Application.sst_crm.restapi.dto.SalaryExpencesDTO;
import Application.sst_crm.restapi.entity.SalaryExpences;
import Application.sst_crm.restapi.service.SalaryExpencesService;


//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/salaryExpences")
public class SalaryExpencesController {
	@Autowired
	private SalaryExpencesService  salaryExpencesService;;
	@GetMapping(produces="application/json")
	public List<SalaryExpences> getsalaryExpences() {
		
		return salaryExpencesService.getSalaryExpences();
		
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<SalaryExpences> getSalaryExpencesById(@PathVariable("id") Long id){
		
		return salaryExpencesService.getSalaryExpencesById(id);
	}
	@PostMapping(consumes="application/json")
	public ResponseEntity<String>createSalaryExpences(@RequestBody SalaryExpencesDTO salaryExpences)
	{
		String response = salaryExpencesService.insertSalaryExpences(salaryExpences);
		return ResponseEntity.ok(response);
	}
	@PutMapping("/{id}")
		public String updateSalaryExpences(@RequestBody SalaryExpencesDTO salaryExpences, @PathVariable("id") Long id) {
			System.out.println("id is"+id);
			return salaryExpencesService.updateSalaryExpences(salaryExpences,id);
			
		}
		
		
		@DeleteMapping("/{id}")
		public String deleteSalaryExpences(@PathVariable("id") Long id )
		{
			return salaryExpencesService.deleteSalaryExpences(id);
		}	
}