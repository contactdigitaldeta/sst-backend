package Application.sst_crm.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.CustomerDTO;

import Application.sst_crm.restapi.entity.Customer;
import Application.sst_crm.restapi.service.CustomerService;
@CrossOrigin(origins="*")

@RestController
@RequestMapping("/customers")

public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	@GetMapping(produces="application/json")
	public List<Customer> getcustomers() {
		
		return customerService.getCustomers();
	}
		@GetMapping(value = "/{id}", produces = "application/json")
		public Optional<Customer> getCustomersById(@PathVariable("id") Long id){
			
			return customerService.getCustomerById(id);
		}
		
		@PostMapping(consumes="application/json")
		public ResponseEntity<String>createCustomer(@RequestBody CustomerDTO customer)
		{
			String response = customerService.insertCustomer(customer);
			return ResponseEntity.ok(response);
			
		}
		
		@PutMapping("/{id}")
		public String updateCustomer(@RequestBody CustomerDTO customer, @PathVariable("id") Long id) {
			System.out.println("id is"+id);
			return customerService.updateCustomer(customer,id);
			
		}
		
		@DeleteMapping("/{id}")
		public String deleteCustomer(@PathVariable("id") Long id )
		{
			return customerService.deleteCustomer(id);
		}

}
