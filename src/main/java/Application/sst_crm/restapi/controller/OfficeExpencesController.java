package Application.sst_crm.restapi.controller;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Application.sst_crm.restapi.dto.OfficeExpencesDTO;
import Application.sst_crm.restapi.entity.OfficeExpences;
import Application.sst_crm.restapi.service.OfficeExpencesServices;


//@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/officeexpenses")
public class OfficeExpencesController {
	
	@Autowired
	private OfficeExpencesServices  officeExpencesService;
	
	@GetMapping(produces="application/json")
	public List<OfficeExpences> getofficeExpences() {
		
		return officeExpencesService.getOfficeExpences();
		
	}
	

	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<OfficeExpences> getOfficeExpencesById(@PathVariable("id") Long id){
		
		return officeExpencesService.getOfficeExpencesById(id);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<String> createOfficeExpences(@RequestBody OfficeExpencesDTO officeExpences)
	{
		String response = officeExpencesService.insertOfficeExpences(officeExpences);
		return ResponseEntity.ok(response);
		
	}
	
	@PutMapping("/{id}")
	public String updateOfficeExpences(@RequestBody OfficeExpencesDTO officeExpences, @PathVariable("id") Long id) {
		System.out.println("id is"+id);
		return officeExpencesService.updateOfficeExpences(officeExpences,id);
		
	}
	@DeleteMapping("/{id}")
	public String deleteOfficeExpences(@PathVariable("id") Long id )
	{
		return officeExpencesService.deleteOfficeExpences(id);
	}
}
